from unittest import TestCase

from DataCleaning import DataCleaner


class TestDataCleaner(TestCase):
    test_text = 'rt bLa BlA !../ testTEST @mention @mention @mention d d s s test'

    def test_clean_text(self):
        self.assertEqual("bla bla testtest test", DataCleaner.clean_text(self.test_text))

    def test_remove_excluded(self):
        self.assertEqual(" ", DataCleaner.remove_excluded(self.test_text, ['test']))
        self.assertEqual(self.test_text, DataCleaner.remove_excluded(self.test_text, ['test3']))

    def test_remove_synonyms(self):
        dc = DataCleaner([], synonyms=['test'], inplaced_synonyms='test2')
        self.assertEqual('rt bLa BlA !../ testTEST @mention @mention @mention d d s s test2', dc.remove_synonyms(self.test_text))

    def test_check_if_text_contain_some_of_must_have_words(self):
        self.assertEqual(self.test_text, DataCleaner.check_if_text_contain_some_of_must_have_words(self.test_text, ['test']))
        self.assertEqual(" ", DataCleaner.check_if_text_contain_some_of_must_have_words(self.test_text, ['test3']))

    def test_remove_by_texts(self):
        self.assertEqual(" ", DataCleaner.remove_by_texts("by test"))
        self.assertEqual(self.test_text, DataCleaner.remove_by_texts(self.test_text))

    def test_check_text_length(self):
        dc = DataCleaner([], text_min_length=10)
        self.assertEqual(" ", dc.check_text_length("by test"))
        self.assertEqual(self.test_text, dc.check_text_length(self.test_text))
