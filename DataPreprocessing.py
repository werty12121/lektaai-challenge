import nltk
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score, KFold
from sklearn.svm import LinearSVC

from DataCleaning import DataCleaner

nltk.download('wordnet')
nltk.download('stopwords')


# TODO Add Hyper Parameters Optimization

class DataPreprocessor:
    def __init__(self, model_n_estimators: int = 20, max_features: int = 2500, min_df: int = 15, max_df: float = 0.7, validation_split: int = 5):
        self.model_n_estimators = model_n_estimators
        self.max_features = max_features
        self.min_df = min_df
        self.max_df = max_df
        self.vectorizer = TfidfVectorizer(max_features=self.max_features, min_df=self.min_df, max_df=self.max_df, stop_words=stopwords.words('english'))
        self.vectorizer_status = None
        self.validation_split = validation_split
        self.model = None

    @staticmethod
    def read_data(classes: list):
        data = []
        data_labels = []
        for i, clas in enumerate(classes):
            readed_data = pd.read_csv(clas + '.csv')
            data.append(readed_data)
            data_labels.append(np.asarray([[i for x in readed_data.text]]).T)
        data = pd.concat(data)
        data_labels = np.concatenate(data_labels, axis=0).ravel()
        return data, data_labels

    @staticmethod
    def tokenize(data: pd.Series):
        stemmer = WordNetLemmatizer()
        data_tokenized = []
        for text in data:
            text = text.split()
            text = [stemmer.lemmatize(word) for word in text]
            text = ' '.join(text)
            data_tokenized.append(text)
        return data_tokenized

    @staticmethod
    def accucacy_cv(model, data, label, kfold: KFold):
        return cross_val_score(model, data, label, scoring="f1", cv=kfold.get_n_splits(data))

    def fit_vectorizer(self, data):
        self.vectorizer = TfidfVectorizer(max_features=self.max_features, min_df=self.min_df, max_df=self.max_df, ngram_range=(1, 2), stop_words=stopwords.words('english'))
        self.vectorizer.fit_transform(data)
        print(self.vectorizer.get_feature_names()[500:1000])
        self.vectorizer_status = 'Fitted'

    def vectorize(self, data):
        if self.vectorizer_status != 'Fitted':
            self.fit_vectorizer(data)
        return self.vectorizer.transform(data).toarray()

    def preprocess_data(self, classes: list):
        data, data_labels = self.read_data(classes)
        data = self.tokenize(data.text)
        data = self.vectorize(data)
        return data, data_labels

    def get_better_model(self, models, data, data_labels, kfolds):
        best_score, best_model = 0, None
        for model in models:
            cv_score = self.accucacy_cv(model, data, data_labels, kfolds)
            print(model.__class__.__name__ + ". Model cross validation mean score: " + str(cv_score.mean()))
            print(model.__class__.__name__ + ". Model cross validation score standard deviation : " + str(cv_score.std()))
            if cv_score.mean() > best_score:
                best_score = cv_score.mean()
                best_model = model
        return best_model, best_score

    def get_trained_model(self, classes: list):
        data, data_labels = self.preprocess_data(classes)
        kfolds = KFold(n_splits=self.validation_split, shuffle=True, random_state=0)
        models = [
            RandomForestClassifier(n_estimators=self.model_n_estimators, random_state=0),
            LogisticRegression(random_state=0),
            # SVC(random_state=0),
            LinearSVC(random_state=0),
            # NuSVC(random_state=0)
        ]
        self.model, score = self.get_better_model(models, data, data_labels, kfolds)
        print(self.model.__class__.__name__ + ". Best model cross validation mean score: " + str(score))
        return self.model.fit(data, data_labels)

    def predict(self, text: str, data_cleaner: DataCleaner):
        if self.model is None:
            raise ValueError("You need to train model first. use get_trained_model function.")
        data = pd.DataFrame(data=[text], columns=['text'])
        data.text = data.text.apply(lambda elem: data_cleaner.clean_text(elem))
        data.text = data.text.apply(lambda elem: data_cleaner.remove_synonyms(elem))
        data = self.tokenize(data.text)
        data = self.vectorize(data)
        prediction = self.model.predict(data)
        print(data_cleaner.classes[prediction[0]])
        return prediction
