import os
import re
import threading
from dataclasses import dataclass

import pandas as pd
import requests
import tweepy
from bs4 import BeautifulSoup as bs

# consumer_key = 'WDQDewjLjDPliNPxv3jQJxhqW'
# consumer_secret = 'S3bJEvDO7DzW8GiPfmAIZ2tM9FZVBRCMc6myYCnt7bwz0ViIqm'
# access_token = '928670739368169472-uZYW9HeOCIkYDwen5KD7bMRnYyxTpud'
# access_token_secret = 'sWG9QMwwEH9j3VZc8gEZC2L8Y8XeGx1KXkHwjnzCroJeE'

# consumer_key = 'Hm1MH48HWZFi7tVHT3acx2vni'
# consumer_secret = '6SMOJKt3HHnIjKTxQUmjIAtuM6nuDfZQcXT9xDhi560CbkTMo2'
# access_token = '928670739368169472-j6Ml6JYkGor5QuNW370N1XxNTGQSdVS'
# access_token_secret = 'EDDc7GGyXuTkwS6kdbYnAitEzUmU6nLMqj6A0olirFYad'

consumer_key = 'sT3y6tzKtQirprWFkz9cMDiDi'
consumer_secret = 'mRnXj7cn4xH8N2UkcKZTOieMkngEOf7yGxB65GJ4CUnIaB7WuM'
access_token = '928670739368169472-NFmzdqYS5FRTQY0SfnzcxlYqbkiykwt'
access_token_secret = 'L7pY2kKpU4wMfV6lbdEGqgEhYuaCK9i1zMxdbTlzhQt6H'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)


def get_tweets(query: str, count: int = 10, save: bool = True, folder: str = '.'):
    try:
        data = [x.full_text.encode("utf-8") for x in tweepy.Cursor(api.search, q=query + " -filter:retweets", lang='en', include_entities=False, tweet_mode='extended', wait_on_rate_limit=True).items(count)]
        data = pd.DataFrame(data, columns=['text'])
        if save:
            data.to_csv(folder + '/raw_tweets ' + re.sub(r'[^a-zA-Z\s]', '', query) + '.csv')
        return data
    except tweepy.TweepError as e:
        print(e.reason)


@dataclass
class TargetWebsite:
    url: str
    how_deep: int = 3
    contains: str = None
    main_site_address: str = None
    excluded: list = None
    is_question_mark_allowed: bool = True
    split_by_dot: bool = False


class WebsiteCrawler:

    def __init__(self, target: TargetWebsite):
        self.url = target.url
        self.how_deep = target.how_deep
        self.contains = target.contains
        self.main_site_address = target.main_site_address
        self.excluded = target.excluded if target.excluded is not None else []
        self.is_question_mark_allowed = target.is_question_mark_allowed
        self.split_by_dot = target.split_by_dot
        self.gathered_data = pd.DataFrame(columns=['text'])
        self.already_crawled = []
        self.site_to_crawl = []
        self.site_to_crawl_deep = []

    @staticmethod
    def tag_visible(element):
        return element.parent.name not in ['style', 'script', 'head', 'title', 'meta', '[document]']

    @staticmethod
    def display_visible_html_using_re(text: str):
        return re.sub(r'(<.*?>)', "", text)

    @staticmethod
    def get_text_and_links_from_web(url: str):
        try:
            request = requests.get(url)
            soup = bs(request.text, 'html.parser')
            text = soup.findAll(text=True)
            links = soup.findAll('a', attrs={'href': re.compile(".*")})
            return text, links
        except requests.exceptions.RequestException as e:
            print(e)
            return [], []

    def start_crawling(self, directory: str, min_text_length: int = 20, text_must_have: list = None):
        how_many_crawled = self.crawl_site(self.url, directory, self.how_deep, min_text_length, text_must_have)
        print(str(how_many_crawled) + " sites crawled.")
        self.save_data_to_csv(directory)

    def save_data_to_csv(self, directory: str):
        print("-DATA-SAVE-")
        self.gathered_data.drop_duplicates(subset='text', inplace=True)
        self.gathered_data.to_csv(directory, index=False)

    def clean_url(self, url: str, mother_url: str):
        if len(url) <= 0:
            return None
        mother_url = mother_url + '/' if len(mother_url.split('/')) < 4 else mother_url
        url = '/'.join(mother_url.split('/')[:-1]) + url[1:] if url[0] == '.' else url
        if url[0] == '/' or 'http' not in url:
            url = url if url[0] == '/' else '/' + url
            if self.main_site_address is None:
                url = '/'.join(mother_url.split('/')[:3]) + url
            else:
                url = '/'.join(self.main_site_address.split('/')[:3]) + url
        url = url.split('#')[0]
        url = url.split('?')[0] if not self.is_question_mark_allowed else url
        url = url.lstrip()
        url = url.rstrip()
        contain = (self.contains is not None and self.contains in url) or self.contains is None
        if url not in self.already_crawled and '.jpg' not in url and '.mp4' not in url and '.png' not in url and contain:
            return url if not any(ex in url for ex in self.excluded) else None

    def crawl_site(self, url: str, directory: str, how_deep: int = 5, min_text_length: int = 20, text_must_have: list = None):

        how_many_site_visited = 0
        self.site_to_crawl.append(url)
        self.site_to_crawl_deep.append(how_deep)
        self.already_crawled.append(url)

        while len(self.site_to_crawl) > 0:

            how_many_site_visited += 1
            if how_many_site_visited % 100 == 0:
                self.save_data_to_csv(directory)

            url_to_crawl = self.site_to_crawl.pop(0)
            how_deep_to_crawl = self.site_to_crawl_deep.pop(0)
            print(url_to_crawl, how_deep_to_crawl)

            text_unfiltered, links = self.get_text_and_links_from_web(url_to_crawl)
            visible_texts = filter(self.tag_visible, text_unfiltered)
            for text in visible_texts:
                text = self.display_visible_html_using_re(text)
                text_must_have_condition = False
                if text_must_have is not None:
                    for t in text_must_have:
                        if t in text.lower():
                            text_must_have_condition = True
                if len(text) >= min_text_length and (text_must_have_condition or text_must_have is None):
                    if self.split_by_dot or len(text.split()) > 250:
                        text = pd.DataFrame(text.split('. '), columns=['text'])
                        self.gathered_data = self.gathered_data.append([text])
                    else:
                        self.gathered_data = self.gathered_data.append({'text': text}, ignore_index=True)
            for link in links:
                url_to_crawl_after = self.clean_url(link.get('href'), url_to_crawl)
                if url_to_crawl_after is not None and how_deep_to_crawl != 0:
                    self.site_to_crawl.append(url_to_crawl_after)
                    self.site_to_crawl_deep.append(how_deep_to_crawl - 1)
                    self.already_crawled.append(url_to_crawl_after)

        return how_many_site_visited


class DataGatherer:
    def __init__(self, classes: list, sites: list = None, twitter: list = None, text_must_have: list = None):
        self.classes = classes
        self.sites = sites
        self.twitter = twitter
        self.text_must_have = text_must_have

    def crawl_sites(self, sites: list, class_name: str):
        threads = []
        if sites is None:
            return
        for site in sites:
            thread = threading.Thread(target=lambda: WebsiteCrawler(site).start_crawling(class_name + '/' + re.sub(r'[^a-zA-Z\s]', '', site.url) + '.csv', text_must_have=self.text_must_have))
            thread.start()
            threads.append(thread)
        for thread in threads:
            thread.join()
        return True

    @staticmethod
    def get_tweets(tweeter_queries: list, class_name: str, twitter_max: int):
        if tweeter_queries is None:
            return
        for query in tweeter_queries:
            get_tweets(query, folder=class_name, count=twitter_max)

    def gather_data(self, twitter_max: int = 1000):
        for class_name, class_sites, class_twitter in zip(self.classes, self.sites, self.twitter):
            try:
                os.mkdir(class_name)
            except FileExistsError as e:
                print(e)
            print("Starting to gather data from web... " + class_name)
            self.crawl_sites(class_sites, class_name)
            print("Web data gathered. " + class_name)
            print("Started gathering data from twitter..." + class_name)
            self.get_tweets(class_twitter, class_name, twitter_max)
            print("Twitter data gathered. " + class_name)
