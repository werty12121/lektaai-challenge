from unittest import TestCase

from bs4 import BeautifulSoup as bs

from DataGathering import WebsiteCrawler, TargetWebsite


class TestWebsiteCrawler(TestCase):
    def test_tag_visible(self):
        html = '<html><style>style<style/> <script>script<script/><head>head<head/> <title>title</title><meta>meta</meta><a>a</a><div>div</div>'
        soup = bs(html, 'html.parser')
        text = soup.findAll(text=True)
        test_texts = filter(WebsiteCrawler.tag_visible, text)
        excepted_text = ['<a>a</a>', '<div>div</div>']
        for test, excepted in zip(test_texts, excepted_text):
            self.assertEqual(excepted, test)

    def test_display_visible_html_using_re(self):
        test_texts = ['<style href="test.html">asd<style/>', '<script>dsa<script/>', '<meta href="test2.html">sda</meta>']
        excepted_text = ['asd', 'dsa', 'sda']
        for test, excepted in zip(test_texts, excepted_text):
            self.assertEqual(excepted, WebsiteCrawler.display_visible_html_using_re(test))

    def test_clean_url(self):
        target_website = TargetWebsite('test', 3, 'url_thats_need_to_be_cleaned', 'http://www.testurl.url', ['excluded_text'], False)
        crawler = WebsiteCrawler(target_website)
        crawler.already_crawled.append('http://www.testurl.url/url_thats_need_to_be_cleaned_but_already_crawled')
        test_urls = ['http://www.testurl.url/url_thats_need_to_be_cleaned?page=1',
                     'http://www.testurl.url/url_thats_need_to_be_cleaned_but_already_crawled?page=1',
                     '/url_thats_need_to_be_cleaned2?page=1',
                     './url_thats_need_to_be_cleaned3?page=1',
                     'http://www.testurl.url/url_thats_need_to_be_cleaned_excluded_text?page=1']
        excepted_url = ['http://www.testurl.url/url_thats_need_to_be_cleaned',
                        None,
                        'http://www.testurl.url/url_thats_need_to_be_cleaned2',
                        'http://www.testurl.url/url_thats_need_to_be_cleaned3',
                        None]

        for test_url, excepted_url in zip(test_urls, excepted_url):
            test_result = crawler.clean_url(test_url, "http://www.testurl.url")
            self.assertEqual(excepted_url, test_result)
